$(document).ready(function() {

    $("#btn_ajout_article").on("click", function() {
        $('#modal_ajout_article').modal({ backdrop: 'static', keyboard: false });
    })

    $("#btn_valid_ajout_article").on("click", function() {
        let check_input = all_input_set("#modal_ajout_article")
        let icon = "error"
        let title = "Ajout article échèc"
        if (!check_input) {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Champs obligatoire vide',
                showConfirmButton: false,
                timer: 2000
            })
        }
        let article_nom = $("#input_article_nom").val()
        let article_prix = $("#input_article_prix").val()
        let article_description = $("#input_article_description").val()
        $.ajax({
            type: "POST",
            url: "validation_ajout_article",
            data: { article_nom: article_nom, article_prix: article_prix, article_description: article_description },
            success: function(result) {
                if (result == "1") {
                    icon = "success"
                    title = "Article ajouté"
                }
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: title,
                    showConfirmButton: false,
                    timer: 2000
                })
                document.location = document.location
            },
            error: function() {}
        });
    })

    $(".btn_suppr_article").on('click', function() {
        let icon = "error"
        let title = "Echèc suppression article"
        Swal.fire({
            title: 'Supprimé un article',
            text: "Êtes-vous sûr de vouloir supprimer?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OUI',
            cancelButtonText: 'NON',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: "suppresion_article",
                    data: { article_id: $(this).val() },
                    success: function(data) {
                        if (data == "1") {
                            icon = "success"
                            title = "Article supprimé"
                        }
                        Swal.fire({
                            position: 'top-end',
                            icon: icon,
                            title: title,
                            showConfirmButton: false,
                            timer: 2000
                        })
                        document.location = document.location
                    },
                    error: function() {}
                });
            }
        })
    })

    $(".btn_modif_article").on('click', function() {
        // let icon = "error"
        // let title = "Echèc récupération article"
        $.ajax({
            type: "POST",
            url: "modification_article",
            dataType: 'json',
            data: { article_id: $(this).val() },
            success: function(data) {
                console.log(Object.keys(data).length)
                if (Object.keys(data).length > 0) {
                    console.log(data.article_description)
                    $('#input_modif_article_nom').val(data.article_nom)
                    $('#input_modif_article_prix').val(data.article_prix)
                    $('#input_modif_article_description').val(data.article_description)
                    $('#btn_valid_modif_article').val(data.article_id)

                    $('#modal_modif_article').modal({ backdrop: 'static', keyboard: false });

                    // icon = "success"
                    // title = "Article modifié"
                }
                // Swal.fire({
                //         position: 'top-end',
                //         icon: icon,
                //         title: title,
                //         showConfirmButton: false,
                //         timer: 2000
                //     })
                // document.location = document.location
            },
            error: function() {}
        });
    })


    $("#btn_valid_modif_article").on('click', function() {
        let icon = "error"
        let title = "Echèc modification article"

        $.ajax({
            type: "POST",
            url: "validation_modif_article",
            data: { article_id: $(this).val(), article_nom: $('#input_modif_article_nom').val(), article_prix: $('#input_modif_article_prix').val(), article_description: $('#input_modif_article_description').val() },
            success: function(data) {
                if (data == "1") {
                    icon = "success"
                    title = "Article modifié"
                }
                Swal.fire({
                    position: 'top-end',
                    icon: icon,
                    title: title,
                    showConfirmButton: false,
                    timer: 2000
                })
                document.location = document.location
            },
            error: function() {}
        });
    })

    function all_input_set(id_parent) {
        let empty = 0;
        let inputs = $(id_parent + " input");
        $.each(inputs, function() {
            if ($(this).val().length === 0) {
                empty++;
                $(this).addClass("is-invalid");
            } else {
                $(this).removeClass("is-invalid").addClass("is-valid");
            }
        });
        return empty == 0 ? true : false
    }

})