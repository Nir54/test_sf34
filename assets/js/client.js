$(document).ready(function() {
    $('#link_registration').on('click', function(e) {

        $('#modal_registration_client').modal({ backdrop: 'static', keyboard: false });
        // Swal.fire('Any fool can use a computer')

        // const { value: formValues } = Swal.fire({
        //     heightAuto: true,
        //     customClass: {
        //         content: 'content-sweet-alert',
        //         htmlContainer: 'test-htmlContainer-class',
        //         container: 'test-container-class',
        //         popup: 'test-popup-class'
        //     },
        //     title: 'Enregistrement utilisateur',
        //     html: '<div class="form-group row">' +
        //         '<label for="input_mail" class="col-md-4 col-form-label">Adresse mail</label>' +
        //         '<div class="col-md-8">' +
        //         '<input type="text" class="form-control" id="input_mail" placeholder="Votre adresse mail">' +
        //         '</div>' +
        //         '</div>' +
        //         '<div class="form-group row mb-0">' +
        //         '<label for="input_mdp" class="col-md-4 col-form-label">Mot de passe</label>' +
        //         '<div class="col-md-8">' +
        //         '<input type="password" class="form-control" id="input_mdp" placeholder="Votre mot de passe">' +
        //         '</div>' +
        //         '</div>' +
        //         '</div>',
        //     focusConfirm: false,
        //     preConfirm: () => {
        //         return [
        //             document.getElementById('swal-input1').value,
        //             document.getElementById('swal-input2').value
        //         ]
        //     }
        // })

        // if (formValues) {
        //     Swal.fire(JSON.stringify(formValues))
        // }

    })

    $('#btn_valid_inscription_client').on("click", function() {
        let check_input = all_input_set("#modal_registration_client")
        if (!check_input) {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Champs obligatoire vide',
                showConfirmButton: false,
                timer: 2000
            })
        } else {
            console.log('mail no vide')
            let client_nom = $("#input_nom_client").val()
            let client_mail = $("#input_mail_client").val()
            let client_mdp = $("#input_mdp_client").val()

            if (IsEmail(client_mail) == false) {
                $(this).addClass("is-invalid");
            } else {
                console.log('mail valide')
                $(this).removeClass("is-invalid").addClass("is-valid");
                $.ajax({
                    type: "POST",
                    url: "validation_inscription_client",
                    data: { client_nom: client_nom, client_mail: client_mail, client_mdp: client_mdp },
                    success: function(result) {
                        if (result == "1") {
                            icon = "success"
                            title = "Client ajouté"
                        } else {
                            icon = "error"
                            title = "Adresse mail existe déjà"
                        }
                        Swal.fire({
                            position: 'top-end',
                            icon: icon,
                            title: title,
                            showConfirmButton: false,
                            timer: 2000
                        })
                        document.location = document.location
                    },
                    error: function() {}
                });
            }

        }

    })

    $('#btn_authentification').on('click', function() {
        let check_input = all_input_set("#content_authentification")
        if (!check_input) {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Champs obligatoire vide',
                showConfirmButton: false,
                timer: 2000
            })
        } else {
            let client_mail = $('#input_mail').val()
            let client_mdp = $('#input_mdp').val()
            $.ajax({
                type: "POST",
                url: "authentification_client",
                data: { client_mail: client_mail, client_mdp: client_mdp },
                success: function(result) {
                    if (result == "ok") {
                        Swal.fire({
                            position: 'top-end',
                            icon: "success",
                            title: "Authentification validé",
                            showConfirmButton: false,
                            timer: 2000
                        })
                        document.location = "article"
                    } else if (result == "ko") {
                        Swal.fire({
                            position: 'top-end',
                            icon: "error",
                            title: "Authentification non validé",
                            showConfirmButton: false,
                            timer: 2000
                        })
                        document.location = document.location
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            icon: "error",
                            title: "Utilisateur non client",
                            showConfirmButton: false,
                            timer: 2000
                        })
                        document.location = document.location
                    }
                },
                error: function() {}
            });
        }
    })


    $('#input_mail_client').on('blur', function() {
        if (IsEmail($(this).val()) == false) {
            $(this).addClass("is-invalid");
            $(this).fadeIn()
        } else {
            $(this).removeClass("is-invalid").addClass("is-valid");
        }
    })

    function all_input_set(id_parent) {
        let empty = 0;
        let inputs = $(id_parent + " input");
        $.each(inputs, function() {
            if ($(this).val().length === 0) {
                empty++;
                $(this).addClass("is-invalid");
            } else {
                $(this).removeClass("is-invalid").addClass("is-valid");
            }
        });
        return empty == 0 ? true : false
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }
})