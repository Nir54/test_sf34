<?php

namespace App\Controller;

use PHPUnit\Util\Json;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{

    /**
     * Récupéré tous les artciles dans la base de données
     * @Route("/article", name="article")
     */
    public function liste_article()
    {
        $client_data = $_SESSION['client'];
        $list_articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        return $this->render('article/liste_article.html.twig', [
            'list_articles' => $list_articles,
            'client_data'=>$client_data
        ]);
    }

    /**
     * Ajouter un article
     * @Route("/validation_ajout_article", name="validation_ajout_article")
     */
    public function validation_ajout_article(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            $article_nom  = $request->get('article_nom');
            $article_description  = $request->get('article_description');
            $article_prix  = $request->get('article_prix');

            $article = new Article();
            $article->setArticleNom($article_nom)
                    ->setArticlePrix($article_prix)
                    ->setArticleDescription($article_description);

            $em->persist($article);
            $em->flush();
        }
        return new Response('1');
    }

    /**
      * Supprimé un article via ID article
     * @Route("/suppresion_article", name="suppresion_article")
     */
    public function suppresion_article(Request $request)
    {
        $result = "0";
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            $article_id  = $request->get('article_id');
            $article_data = $this->getDoctrine()->getRepository(Article::class)->find($article_id);
            if($article_data){
                $em->remove($article_data);
                $em->flush();
                $result = "1";
            }
        }
        return new Response($result);
    }

    /**
      * Validation modif d'un article via ID article
     * @Route("/validation_modif_article", name="validation_modif_article")
     */
    public function validation_modif_article(Request $request)
    {
        $result = "0";
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            $article_id  = $request->get('article_id');
            $article_data = $this->getDoctrine()->getRepository(Article::class)->find($article_id);
            if($article_data){
                $article_data->setArticleNom($request->get('article_nom'))
                            ->setArticlePrix($request->get('article_prix'))
                            ->setArticleDescription($request->get('article_description'));
                $em->flush();
                $result = "1";
            }
        }
        return new Response($result);
    }

    /**
      * Modifier un article via ID article
     * @Route("/modification_article", name="modification_article")
     */
    public function modification_article(Request $request)
    {
        $array_article = [];
        if ($request->getMethod() == 'POST') {
            $article_id  = $request->get('article_id');
            $article_data = $this->getDoctrine()->getRepository(Article::class)->find($article_id);

            if($article_data){
                $array_article = [
                    "article_id"=> $article_data->getId(),
                    "article_nom" => $article_data->getArticleNom(),
                    "article_prix" => $article_data->getArticlePrix(),
                    "article_description" => $article_data->getArticleDescription(),
                ];
            }
        }
        return new JsonResponse($array_article);
    }
    
}
