<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClientController extends Controller
{
    /**
     * @Route("/", name="default")
     */
    public function default(): Response
    {
        if (isset($_SESSION)) {
            session_destroy();
        }
        return $this->render('client/authentification.html.twig');
    }

    /**
     * @Route("/validation_inscription_client", name="validation_inscription_client")
     */
    public function validation_inscription_client(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            $client_nom  = $request->get('client_nom');
            $client_mail  = $request->get('client_mail');
            $client_mdp  = password_hash($request->get('client_mdp'), PASSWORD_DEFAULT);
            $client_status = 1;

            if($this->getDoctrine()->getRepository(Client::class)->findBy(["client_mail"=>$client_mail])){
                return new Response('0');
            }

            $chech_nom = explode(".", $client_nom);

            if (isset($chech_nom[1]) && $chech_nom[1] === "gerant") {
                $client_status = 0;
            }

            $client = new Client();
            $client->setClientNom($chech_nom[0])
                    ->setClientMail($client_mail)
                    ->setClientStatus($client_status)
                    ->setClientMdp($client_mdp);

            $em->persist($client);
            $em->flush();
        }
        return new Response('1');

    }

    /**
     * @Route("/authentification_client", name="authentification_client")
     */
    public function authentification_client(Request $request)
    {
        $result = "";
        if ($request->getMethod() == 'POST') {
            $client_mail  = $request->get('client_mail');
            $client_mdp  = $request->get('client_mdp');
            $client_data = $this->getDoctrine()->getRepository(Client::class)->findOneBy(["client_mail"=>$client_mail]);
            session_start();
            $_SESSION["client"]=$client_data;
            if(!$client_data){
                return new Response("no_client");
            }
            $result = password_verify($client_mdp, $client_data->getClientMdp()) ? "ok" : "ko";
        }
        return new Response($result);
    }
}
