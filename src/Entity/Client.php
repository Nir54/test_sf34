<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $client_nom;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $client_mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client_mdp;

    /**
     * @ORM\OneToMany(targetEntity=Commande::class, mappedBy="client_id")
     */
    private $commandes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $client_status;

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
        // $this->client_status = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientNom(): ?string
    {
        return $this->client_nom;
    }

    public function setClientNom(string $client_nom): self
    {
        $this->client_nom = $client_nom;

        return $this;
    }

    public function getClientMail(): ?string
    {
        return $this->client_mail;
    }

    public function setClientMail(string $client_mail): self
    {
        $this->client_mail = $client_mail;

        return $this;
    }

    public function getClientMdp(): ?string
    {
        return $this->client_mdp;
    }

    public function setClientMdp(string $client_mdp): self
    {
        $this->client_mdp = $client_mdp;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setClientId($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getClientId() === $this) {
                $commande->setClientId(null);
            }
        }

        return $this;
    }

    public function getClientStatus(): ?bool
    {
        return $this->client_status;
    }

    public function setClientStatus(bool $client_status): self
    {
        $this->client_status = $client_status;

        return $this;
    }
}
