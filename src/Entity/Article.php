<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $article_nom;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $article_prix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $article_description;

    /**
     * @ORM\OneToMany(targetEntity=Commande::class, mappedBy="article_id")
     */
    private $commandes;

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleNom(): ?string
    {
        return $this->article_nom;
    }

    public function setArticleNom(string $article_nom): self
    {
        $this->article_nom = $article_nom;

        return $this;
    }

    public function getArticlePrix(): ?string
    {
        return $this->article_prix;
    }

    public function setArticlePrix(string $article_prix): self
    {
        $this->article_prix = $article_prix;

        return $this;
    }

    public function getArticleDescription(): ?string
    {
        return $this->article_description;
    }

    public function setArticleDescription(?string $article_description): self
    {
        $this->article_description = $article_description;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setArticleId($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getArticleId() === $this) {
                $commande->setArticleId(null);
            }
        }

        return $this;
    }
}
