<?php

namespace App\Entity;

use App\Repository\CommandeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 */
class Commande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $commande_quatite;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $commande_total_prix;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="commandes")
     */
    private $article_id;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="commandes")
     */
    private $client_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommandeQuatite(): ?int
    {
        return $this->commande_quatite;
    }

    public function setCommandeQuatite(int $commande_quatite): self
    {
        $this->commande_quatite = $commande_quatite;

        return $this;
    }

    public function getCommandeTotalPrix(): ?string
    {
        return $this->commande_total_prix;
    }

    public function setCommandeTotalPrix(string $commande_total_prix): self
    {
        $this->commande_total_prix = $commande_total_prix;

        return $this;
    }

    public function getArticleId(): ?Article
    {
        return $this->article_id;
    }

    public function setArticleId(?Article $article_id): self
    {
        $this->article_id = $article_id;

        return $this;
    }

    public function getClientId(): ?Client
    {
        return $this->client_id;
    }

    public function setClientId(?Client $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }
}
