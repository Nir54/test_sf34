(self["webpackChunk"] = self["webpackChunk"] || []).push([["app"],{

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// assets/js/app.js

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)
// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';
__webpack_require__(/*! ../js/client.js */ "./assets/js/client.js");

__webpack_require__(/*! ../js/article.js */ "./assets/js/article.js");

__webpack_require__(/*! @fortawesome/fontawesome-free/css/all.min.css */ "./node_modules/@fortawesome/fontawesome-free/css/all.min.css");

console.log('Hello Webpack Encore');

/***/ }),

/***/ "./assets/js/article.js":
/*!******************************!*\
  !*** ./assets/js/article.js ***!
  \******************************/
/***/ (() => {

$(document).ready(function () {
  $("#btn_ajout_article").on("click", function () {
    $('#modal_ajout_article').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
  $("#btn_valid_ajout_article").on("click", function () {
    var check_input = all_input_set("#modal_ajout_article");
    var icon = "error";
    var title = "Ajout article échèc";

    if (!check_input) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Champs obligatoire vide',
        showConfirmButton: false,
        timer: 2000
      });
    }

    var article_nom = $("#input_article_nom").val();
    var article_prix = $("#input_article_prix").val();
    var article_description = $("#input_article_description").val();
    $.ajax({
      type: "POST",
      url: "validation_ajout_article",
      data: {
        article_nom: article_nom,
        article_prix: article_prix,
        article_description: article_description
      },
      success: function success(result) {
        if (result == "1") {
          icon = "success";
          title = "Article ajouté";
        }

        Swal.fire({
          position: 'top-end',
          icon: icon,
          title: title,
          showConfirmButton: false,
          timer: 2000
        });
        document.location = document.location;
      },
      error: function error() {}
    });
  });
  $(".btn_suppr_article").on('click', function () {
    var _this = this;

    var icon = "error";
    var title = "Echèc suppression article";
    Swal.fire({
      title: 'Supprimé un article',
      text: "Êtes-vous sûr de vouloir supprimer?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OUI',
      cancelButtonText: 'NON'
    }).then(function (result) {
      if (result.isConfirmed) {
        $.ajax({
          type: "POST",
          url: "suppresion_article",
          data: {
            article_id: $(_this).val()
          },
          success: function success(data) {
            if (data == "1") {
              icon = "success";
              title = "Article supprimé";
            }

            Swal.fire({
              position: 'top-end',
              icon: icon,
              title: title,
              showConfirmButton: false,
              timer: 2000
            });
            document.location = document.location;
          },
          error: function error() {}
        });
      }
    });
  });
  $(".btn_modif_article").on('click', function () {
    // let icon = "error"
    // let title = "Echèc récupération article"
    $.ajax({
      type: "POST",
      url: "modification_article",
      dataType: 'json',
      data: {
        article_id: $(this).val()
      },
      success: function success(data) {
        console.log(Object.keys(data).length);

        if (Object.keys(data).length > 0) {
          console.log(data.article_description);
          $('#input_modif_article_nom').val(data.article_nom);
          $('#input_modif_article_prix').val(data.article_prix);
          $('#input_modif_article_description').val(data.article_description);
          $('#btn_valid_modif_article').val(data.article_id);
          $('#modal_modif_article').modal({
            backdrop: 'static',
            keyboard: false
          }); // icon = "success"
          // title = "Article modifié"
        } // Swal.fire({
        //         position: 'top-end',
        //         icon: icon,
        //         title: title,
        //         showConfirmButton: false,
        //         timer: 2000
        //     })
        // document.location = document.location

      },
      error: function error() {}
    });
  });
  $("#btn_valid_modif_article").on('click', function () {
    var icon = "error";
    var title = "Echèc modification article";
    $.ajax({
      type: "POST",
      url: "validation_modif_article",
      data: {
        article_id: $(this).val(),
        article_nom: $('#input_modif_article_nom').val(),
        article_prix: $('#input_modif_article_prix').val(),
        article_description: $('#input_modif_article_description').val()
      },
      success: function success(data) {
        if (data == "1") {
          icon = "success";
          title = "Article modifié";
        }

        Swal.fire({
          position: 'top-end',
          icon: icon,
          title: title,
          showConfirmButton: false,
          timer: 2000
        });
        document.location = document.location;
      },
      error: function error() {}
    });
  });

  function all_input_set(id_parent) {
    var empty = 0;
    var inputs = $(id_parent + " input");
    $.each(inputs, function () {
      if ($(this).val().length === 0) {
        empty++;
        $(this).addClass("is-invalid");
      } else {
        $(this).removeClass("is-invalid").addClass("is-valid");
      }
    });
    return empty == 0 ? true : false;
  }
});

/***/ }),

/***/ "./assets/js/client.js":
/*!*****************************!*\
  !*** ./assets/js/client.js ***!
  \*****************************/
/***/ (() => {

$(document).ready(function () {
  $('#link_registration').on('click', function (e) {
    $('#modal_registration_client').modal({
      backdrop: 'static',
      keyboard: false
    }); // Swal.fire('Any fool can use a computer')
    // const { value: formValues } = Swal.fire({
    //     heightAuto: true,
    //     customClass: {
    //         content: 'content-sweet-alert',
    //         htmlContainer: 'test-htmlContainer-class',
    //         container: 'test-container-class',
    //         popup: 'test-popup-class'
    //     },
    //     title: 'Enregistrement utilisateur',
    //     html: '<div class="form-group row">' +
    //         '<label for="input_mail" class="col-md-4 col-form-label">Adresse mail</label>' +
    //         '<div class="col-md-8">' +
    //         '<input type="text" class="form-control" id="input_mail" placeholder="Votre adresse mail">' +
    //         '</div>' +
    //         '</div>' +
    //         '<div class="form-group row mb-0">' +
    //         '<label for="input_mdp" class="col-md-4 col-form-label">Mot de passe</label>' +
    //         '<div class="col-md-8">' +
    //         '<input type="password" class="form-control" id="input_mdp" placeholder="Votre mot de passe">' +
    //         '</div>' +
    //         '</div>' +
    //         '</div>',
    //     focusConfirm: false,
    //     preConfirm: () => {
    //         return [
    //             document.getElementById('swal-input1').value,
    //             document.getElementById('swal-input2').value
    //         ]
    //     }
    // })
    // if (formValues) {
    //     Swal.fire(JSON.stringify(formValues))
    // }
  });
  $('#btn_valid_inscription_client').on("click", function () {
    var check_input = all_input_set("#modal_registration_client");

    if (!check_input) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Champs obligatoire vide',
        showConfirmButton: false,
        timer: 2000
      });
    } else {
      console.log('mail no vide');
      var client_nom = $("#input_nom_client").val();
      var client_mail = $("#input_mail_client").val();
      var client_mdp = $("#input_mdp_client").val();

      if (IsEmail(client_mail) == false) {
        $(this).addClass("is-invalid");
      } else {
        console.log('mail valide');
        $(this).removeClass("is-invalid").addClass("is-valid");
        $.ajax({
          type: "POST",
          url: "validation_inscription_client",
          data: {
            client_nom: client_nom,
            client_mail: client_mail,
            client_mdp: client_mdp
          },
          success: function success(result) {
            if (result == "1") {
              icon = "success";
              title = "Client ajouté";
            } else {
              icon = "error";
              title = "Adresse mail existe déjà";
            }

            Swal.fire({
              position: 'top-end',
              icon: icon,
              title: title,
              showConfirmButton: false,
              timer: 2000
            });
            document.location = document.location;
          },
          error: function error() {}
        });
      }
    }
  });
  $('#btn_authentification').on('click', function () {
    var check_input = all_input_set("#content_authentification");

    if (!check_input) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Champs obligatoire vide',
        showConfirmButton: false,
        timer: 2000
      });
    } else {
      var client_mail = $('#input_mail').val();
      var client_mdp = $('#input_mdp').val();
      $.ajax({
        type: "POST",
        url: "authentification_client",
        data: {
          client_mail: client_mail,
          client_mdp: client_mdp
        },
        success: function success(result) {
          if (result == "ok") {
            Swal.fire({
              position: 'top-end',
              icon: "success",
              title: "Authentification validé",
              showConfirmButton: false,
              timer: 2000
            });
            document.location = "article";
          } else if (result == "ko") {
            Swal.fire({
              position: 'top-end',
              icon: "error",
              title: "Authentification non validé",
              showConfirmButton: false,
              timer: 2000
            });
          } else {
            Swal.fire({
              position: 'top-end',
              icon: "error",
              title: "Utilisateur non client",
              showConfirmButton: false,
              timer: 2000
            });
          }
        },
        error: function error() {}
      });
    }
  });
  $('#input_mail_client').on('blur', function () {
    if (IsEmail($(this).val()) == false) {
      $(this).addClass("is-invalid");
      $(this).fadeIn();
    } else {
      $(this).removeClass("is-invalid").addClass("is-valid");
    }
  });

  function all_input_set(id_parent) {
    var empty = 0;
    var inputs = $(id_parent + " input");
    $.each(inputs, function () {
      if ($(this).val().length === 0) {
        empty++;
        $(this).addClass("is-invalid");
      } else {
        $(this).removeClass("is-invalid").addClass("is-valid");
      }
    });
    return empty == 0 ? true : false;
  }

  function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!regex.test(email)) {
      return false;
    } else {
      return true;
    }
  }
});

/***/ }),

/***/ "./node_modules/@fortawesome/fontawesome-free/css/all.min.css":
/*!********************************************************************!*\
  !*** ./node_modules/@fortawesome/fontawesome-free/css/all.min.css ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ var __webpack_exports__ = (__webpack_exec__("./assets/js/app.js"));
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUFBLG1CQUFPLENBQUMsOENBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxnREFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLG1IQUFELENBQVA7O0FBRUFDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHNCQUFaOzs7Ozs7Ozs7O0FDakJBQyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFFekJGLEVBQUFBLENBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCRyxFQUF4QixDQUEyQixPQUEzQixFQUFvQyxZQUFXO0FBQzNDSCxJQUFBQSxDQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQkksS0FBMUIsQ0FBZ0M7QUFBRUMsTUFBQUEsUUFBUSxFQUFFLFFBQVo7QUFBc0JDLE1BQUFBLFFBQVEsRUFBRTtBQUFoQyxLQUFoQztBQUNILEdBRkQ7QUFJQU4sRUFBQUEsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJHLEVBQTlCLENBQWlDLE9BQWpDLEVBQTBDLFlBQVc7QUFDakQsUUFBSUksV0FBVyxHQUFHQyxhQUFhLENBQUMsc0JBQUQsQ0FBL0I7QUFDQSxRQUFJQyxJQUFJLEdBQUcsT0FBWDtBQUNBLFFBQUlDLEtBQUssR0FBRyxxQkFBWjs7QUFDQSxRQUFJLENBQUNILFdBQUwsRUFBa0I7QUFDZEksTUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVU7QUFDTkMsUUFBQUEsUUFBUSxFQUFFLFNBREo7QUFFTkosUUFBQUEsSUFBSSxFQUFFLE9BRkE7QUFHTkMsUUFBQUEsS0FBSyxFQUFFLHlCQUhEO0FBSU5JLFFBQUFBLGlCQUFpQixFQUFFLEtBSmI7QUFLTkMsUUFBQUEsS0FBSyxFQUFFO0FBTEQsT0FBVjtBQU9IOztBQUNELFFBQUlDLFdBQVcsR0FBR2hCLENBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCaUIsR0FBeEIsRUFBbEI7QUFDQSxRQUFJQyxZQUFZLEdBQUdsQixDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QmlCLEdBQXpCLEVBQW5CO0FBQ0EsUUFBSUUsbUJBQW1CLEdBQUduQixDQUFDLENBQUMsNEJBQUQsQ0FBRCxDQUFnQ2lCLEdBQWhDLEVBQTFCO0FBQ0FqQixJQUFBQSxDQUFDLENBQUNvQixJQUFGLENBQU87QUFDSEMsTUFBQUEsSUFBSSxFQUFFLE1BREg7QUFFSEMsTUFBQUEsR0FBRyxFQUFFLDBCQUZGO0FBR0hDLE1BQUFBLElBQUksRUFBRTtBQUFFUCxRQUFBQSxXQUFXLEVBQUVBLFdBQWY7QUFBNEJFLFFBQUFBLFlBQVksRUFBRUEsWUFBMUM7QUFBd0RDLFFBQUFBLG1CQUFtQixFQUFFQTtBQUE3RSxPQUhIO0FBSUhLLE1BQUFBLE9BQU8sRUFBRSxpQkFBU0MsTUFBVCxFQUFpQjtBQUN0QixZQUFJQSxNQUFNLElBQUksR0FBZCxFQUFtQjtBQUNmaEIsVUFBQUEsSUFBSSxHQUFHLFNBQVA7QUFDQUMsVUFBQUEsS0FBSyxHQUFHLGdCQUFSO0FBQ0g7O0FBQ0RDLFFBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05DLFVBQUFBLFFBQVEsRUFBRSxTQURKO0FBRU5KLFVBQUFBLElBQUksRUFBRUEsSUFGQTtBQUdOQyxVQUFBQSxLQUFLLEVBQUVBLEtBSEQ7QUFJTkksVUFBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxVQUFBQSxLQUFLLEVBQUU7QUFMRCxTQUFWO0FBT0FkLFFBQUFBLFFBQVEsQ0FBQ3lCLFFBQVQsR0FBb0J6QixRQUFRLENBQUN5QixRQUE3QjtBQUNILE9BakJFO0FBa0JIQyxNQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQWxCakIsS0FBUDtBQW9CSCxHQXBDRDtBQXNDQTNCLEVBQUFBLENBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCRyxFQUF4QixDQUEyQixPQUEzQixFQUFvQyxZQUFXO0FBQUE7O0FBQzNDLFFBQUlNLElBQUksR0FBRyxPQUFYO0FBQ0EsUUFBSUMsS0FBSyxHQUFHLDJCQUFaO0FBQ0FDLElBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05GLE1BQUFBLEtBQUssRUFBRSxxQkFERDtBQUVOa0IsTUFBQUEsSUFBSSxFQUFFLHFDQUZBO0FBR05uQixNQUFBQSxJQUFJLEVBQUUsU0FIQTtBQUlOb0IsTUFBQUEsZ0JBQWdCLEVBQUUsSUFKWjtBQUtOQyxNQUFBQSxrQkFBa0IsRUFBRSxTQUxkO0FBTU5DLE1BQUFBLGlCQUFpQixFQUFFLE1BTmI7QUFPTkMsTUFBQUEsaUJBQWlCLEVBQUUsS0FQYjtBQVFOQyxNQUFBQSxnQkFBZ0IsRUFBRTtBQVJaLEtBQVYsRUFTR0MsSUFUSCxDQVNRLFVBQUNULE1BQUQsRUFBWTtBQUNoQixVQUFJQSxNQUFNLENBQUNVLFdBQVgsRUFBd0I7QUFDcEJuQyxRQUFBQSxDQUFDLENBQUNvQixJQUFGLENBQU87QUFDSEMsVUFBQUEsSUFBSSxFQUFFLE1BREg7QUFFSEMsVUFBQUEsR0FBRyxFQUFFLG9CQUZGO0FBR0hDLFVBQUFBLElBQUksRUFBRTtBQUFFYSxZQUFBQSxVQUFVLEVBQUVwQyxDQUFDLENBQUMsS0FBRCxDQUFELENBQVFpQixHQUFSO0FBQWQsV0FISDtBQUlITyxVQUFBQSxPQUFPLEVBQUUsaUJBQVNELElBQVQsRUFBZTtBQUNwQixnQkFBSUEsSUFBSSxJQUFJLEdBQVosRUFBaUI7QUFDYmQsY0FBQUEsSUFBSSxHQUFHLFNBQVA7QUFDQUMsY0FBQUEsS0FBSyxHQUFHLGtCQUFSO0FBQ0g7O0FBQ0RDLFlBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05DLGNBQUFBLFFBQVEsRUFBRSxTQURKO0FBRU5KLGNBQUFBLElBQUksRUFBRUEsSUFGQTtBQUdOQyxjQUFBQSxLQUFLLEVBQUVBLEtBSEQ7QUFJTkksY0FBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxjQUFBQSxLQUFLLEVBQUU7QUFMRCxhQUFWO0FBT0FkLFlBQUFBLFFBQVEsQ0FBQ3lCLFFBQVQsR0FBb0J6QixRQUFRLENBQUN5QixRQUE3QjtBQUNILFdBakJFO0FBa0JIQyxVQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQWxCakIsU0FBUDtBQW9CSDtBQUNKLEtBaENEO0FBaUNILEdBcENEO0FBc0NBM0IsRUFBQUEsQ0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JHLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DLFlBQVc7QUFDM0M7QUFDQTtBQUNBSCxJQUFBQSxDQUFDLENBQUNvQixJQUFGLENBQU87QUFDSEMsTUFBQUEsSUFBSSxFQUFFLE1BREg7QUFFSEMsTUFBQUEsR0FBRyxFQUFFLHNCQUZGO0FBR0hlLE1BQUFBLFFBQVEsRUFBRSxNQUhQO0FBSUhkLE1BQUFBLElBQUksRUFBRTtBQUFFYSxRQUFBQSxVQUFVLEVBQUVwQyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQixHQUFSO0FBQWQsT0FKSDtBQUtITyxNQUFBQSxPQUFPLEVBQUUsaUJBQVNELElBQVQsRUFBZTtBQUNwQnpCLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZdUMsTUFBTSxDQUFDQyxJQUFQLENBQVloQixJQUFaLEVBQWtCaUIsTUFBOUI7O0FBQ0EsWUFBSUYsTUFBTSxDQUFDQyxJQUFQLENBQVloQixJQUFaLEVBQWtCaUIsTUFBbEIsR0FBMkIsQ0FBL0IsRUFBa0M7QUFDOUIxQyxVQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWXdCLElBQUksQ0FBQ0osbUJBQWpCO0FBQ0FuQixVQUFBQSxDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QmlCLEdBQTlCLENBQWtDTSxJQUFJLENBQUNQLFdBQXZDO0FBQ0FoQixVQUFBQSxDQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQmlCLEdBQS9CLENBQW1DTSxJQUFJLENBQUNMLFlBQXhDO0FBQ0FsQixVQUFBQSxDQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ2lCLEdBQXRDLENBQTBDTSxJQUFJLENBQUNKLG1CQUEvQztBQUNBbkIsVUFBQUEsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJpQixHQUE5QixDQUFrQ00sSUFBSSxDQUFDYSxVQUF2QztBQUVBcEMsVUFBQUEsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJJLEtBQTFCLENBQWdDO0FBQUVDLFlBQUFBLFFBQVEsRUFBRSxRQUFaO0FBQXNCQyxZQUFBQSxRQUFRLEVBQUU7QUFBaEMsV0FBaEMsRUFQOEIsQ0FTOUI7QUFDQTtBQUNILFNBYm1CLENBY3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0gsT0EzQkU7QUE0QkhxQixNQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQTVCakIsS0FBUDtBQThCSCxHQWpDRDtBQW9DQTNCLEVBQUFBLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCRyxFQUE5QixDQUFpQyxPQUFqQyxFQUEwQyxZQUFXO0FBQ2pELFFBQUlNLElBQUksR0FBRyxPQUFYO0FBQ0EsUUFBSUMsS0FBSyxHQUFHLDRCQUFaO0FBRUFWLElBQUFBLENBQUMsQ0FBQ29CLElBQUYsQ0FBTztBQUNIQyxNQUFBQSxJQUFJLEVBQUUsTUFESDtBQUVIQyxNQUFBQSxHQUFHLEVBQUUsMEJBRkY7QUFHSEMsTUFBQUEsSUFBSSxFQUFFO0FBQUVhLFFBQUFBLFVBQVUsRUFBRXBDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLEdBQVIsRUFBZDtBQUE2QkQsUUFBQUEsV0FBVyxFQUFFaEIsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJpQixHQUE5QixFQUExQztBQUErRUMsUUFBQUEsWUFBWSxFQUFFbEIsQ0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JpQixHQUEvQixFQUE3RjtBQUFtSUUsUUFBQUEsbUJBQW1CLEVBQUVuQixDQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ2lCLEdBQXRDO0FBQXhKLE9BSEg7QUFJSE8sTUFBQUEsT0FBTyxFQUFFLGlCQUFTRCxJQUFULEVBQWU7QUFDcEIsWUFBSUEsSUFBSSxJQUFJLEdBQVosRUFBaUI7QUFDYmQsVUFBQUEsSUFBSSxHQUFHLFNBQVA7QUFDQUMsVUFBQUEsS0FBSyxHQUFHLGlCQUFSO0FBQ0g7O0FBQ0RDLFFBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05DLFVBQUFBLFFBQVEsRUFBRSxTQURKO0FBRU5KLFVBQUFBLElBQUksRUFBRUEsSUFGQTtBQUdOQyxVQUFBQSxLQUFLLEVBQUVBLEtBSEQ7QUFJTkksVUFBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxVQUFBQSxLQUFLLEVBQUU7QUFMRCxTQUFWO0FBT0FkLFFBQUFBLFFBQVEsQ0FBQ3lCLFFBQVQsR0FBb0J6QixRQUFRLENBQUN5QixRQUE3QjtBQUNILE9BakJFO0FBa0JIQyxNQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQWxCakIsS0FBUDtBQW9CSCxHQXhCRDs7QUEwQkEsV0FBU25CLGFBQVQsQ0FBdUJpQyxTQUF2QixFQUFrQztBQUM5QixRQUFJQyxLQUFLLEdBQUcsQ0FBWjtBQUNBLFFBQUlDLE1BQU0sR0FBRzNDLENBQUMsQ0FBQ3lDLFNBQVMsR0FBRyxRQUFiLENBQWQ7QUFDQXpDLElBQUFBLENBQUMsQ0FBQzRDLElBQUYsQ0FBT0QsTUFBUCxFQUFlLFlBQVc7QUFDdEIsVUFBSTNDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLEdBQVIsR0FBY3VCLE1BQWQsS0FBeUIsQ0FBN0IsRUFBZ0M7QUFDNUJFLFFBQUFBLEtBQUs7QUFDTDFDLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZDLFFBQVIsQ0FBaUIsWUFBakI7QUFDSCxPQUhELE1BR087QUFDSDdDLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUThDLFdBQVIsQ0FBb0IsWUFBcEIsRUFBa0NELFFBQWxDLENBQTJDLFVBQTNDO0FBQ0g7QUFDSixLQVBEO0FBUUEsV0FBT0gsS0FBSyxJQUFJLENBQVQsR0FBYSxJQUFiLEdBQW9CLEtBQTNCO0FBQ0g7QUFFSixDQTlKRDs7Ozs7Ozs7OztBQ0FBMUMsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFXO0FBQ3pCRixFQUFBQSxDQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QkcsRUFBeEIsQ0FBMkIsT0FBM0IsRUFBb0MsVUFBUzRDLENBQVQsRUFBWTtBQUU1Qy9DLElBQUFBLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDSSxLQUFoQyxDQUFzQztBQUFFQyxNQUFBQSxRQUFRLEVBQUUsUUFBWjtBQUFzQkMsTUFBQUEsUUFBUSxFQUFFO0FBQWhDLEtBQXRDLEVBRjRDLENBRzVDO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUgsR0F4Q0Q7QUEwQ0FOLEVBQUFBLENBQUMsQ0FBQywrQkFBRCxDQUFELENBQW1DRyxFQUFuQyxDQUFzQyxPQUF0QyxFQUErQyxZQUFXO0FBQ3RELFFBQUlJLFdBQVcsR0FBR0MsYUFBYSxDQUFDLDRCQUFELENBQS9COztBQUNBLFFBQUksQ0FBQ0QsV0FBTCxFQUFrQjtBQUNkSSxNQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNOQyxRQUFBQSxRQUFRLEVBQUUsU0FESjtBQUVOSixRQUFBQSxJQUFJLEVBQUUsT0FGQTtBQUdOQyxRQUFBQSxLQUFLLEVBQUUseUJBSEQ7QUFJTkksUUFBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxRQUFBQSxLQUFLLEVBQUU7QUFMRCxPQUFWO0FBT0gsS0FSRCxNQVFPO0FBQ0hqQixNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxjQUFaO0FBQ0EsVUFBSWlELFVBQVUsR0FBR2hELENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCaUIsR0FBdkIsRUFBakI7QUFDQSxVQUFJZ0MsV0FBVyxHQUFHakQsQ0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JpQixHQUF4QixFQUFsQjtBQUNBLFVBQUlpQyxVQUFVLEdBQUdsRCxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QmlCLEdBQXZCLEVBQWpCOztBQUVBLFVBQUlrQyxPQUFPLENBQUNGLFdBQUQsQ0FBUCxJQUF3QixLQUE1QixFQUFtQztBQUMvQmpELFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZDLFFBQVIsQ0FBaUIsWUFBakI7QUFDSCxPQUZELE1BRU87QUFDSC9DLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVo7QUFDQUMsUUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEMsV0FBUixDQUFvQixZQUFwQixFQUFrQ0QsUUFBbEMsQ0FBMkMsVUFBM0M7QUFDQTdDLFFBQUFBLENBQUMsQ0FBQ29CLElBQUYsQ0FBTztBQUNIQyxVQUFBQSxJQUFJLEVBQUUsTUFESDtBQUVIQyxVQUFBQSxHQUFHLEVBQUUsK0JBRkY7QUFHSEMsVUFBQUEsSUFBSSxFQUFFO0FBQUV5QixZQUFBQSxVQUFVLEVBQUVBLFVBQWQ7QUFBMEJDLFlBQUFBLFdBQVcsRUFBRUEsV0FBdkM7QUFBb0RDLFlBQUFBLFVBQVUsRUFBRUE7QUFBaEUsV0FISDtBQUlIMUIsVUFBQUEsT0FBTyxFQUFFLGlCQUFTQyxNQUFULEVBQWlCO0FBQ3RCLGdCQUFJQSxNQUFNLElBQUksR0FBZCxFQUFtQjtBQUNmaEIsY0FBQUEsSUFBSSxHQUFHLFNBQVA7QUFDQUMsY0FBQUEsS0FBSyxHQUFHLGVBQVI7QUFDSCxhQUhELE1BR087QUFDSEQsY0FBQUEsSUFBSSxHQUFHLE9BQVA7QUFDQUMsY0FBQUEsS0FBSyxHQUFHLDBCQUFSO0FBQ0g7O0FBQ0RDLFlBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05DLGNBQUFBLFFBQVEsRUFBRSxTQURKO0FBRU5KLGNBQUFBLElBQUksRUFBRUEsSUFGQTtBQUdOQyxjQUFBQSxLQUFLLEVBQUVBLEtBSEQ7QUFJTkksY0FBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxjQUFBQSxLQUFLLEVBQUU7QUFMRCxhQUFWO0FBT0FkLFlBQUFBLFFBQVEsQ0FBQ3lCLFFBQVQsR0FBb0J6QixRQUFRLENBQUN5QixRQUE3QjtBQUNILFdBcEJFO0FBcUJIQyxVQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQXJCakIsU0FBUDtBQXVCSDtBQUVKO0FBRUosR0FoREQ7QUFrREEzQixFQUFBQSxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQkcsRUFBM0IsQ0FBOEIsT0FBOUIsRUFBdUMsWUFBVztBQUM5QyxRQUFJSSxXQUFXLEdBQUdDLGFBQWEsQ0FBQywyQkFBRCxDQUEvQjs7QUFDQSxRQUFJLENBQUNELFdBQUwsRUFBa0I7QUFDZEksTUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVU7QUFDTkMsUUFBQUEsUUFBUSxFQUFFLFNBREo7QUFFTkosUUFBQUEsSUFBSSxFQUFFLE9BRkE7QUFHTkMsUUFBQUEsS0FBSyxFQUFFLHlCQUhEO0FBSU5JLFFBQUFBLGlCQUFpQixFQUFFLEtBSmI7QUFLTkMsUUFBQUEsS0FBSyxFQUFFO0FBTEQsT0FBVjtBQU9ILEtBUkQsTUFRTztBQUNILFVBQUlrQyxXQUFXLEdBQUdqRCxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCaUIsR0FBakIsRUFBbEI7QUFDQSxVQUFJaUMsVUFBVSxHQUFHbEQsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQmlCLEdBQWhCLEVBQWpCO0FBQ0FqQixNQUFBQSxDQUFDLENBQUNvQixJQUFGLENBQU87QUFDSEMsUUFBQUEsSUFBSSxFQUFFLE1BREg7QUFFSEMsUUFBQUEsR0FBRyxFQUFFLHlCQUZGO0FBR0hDLFFBQUFBLElBQUksRUFBRTtBQUFFMEIsVUFBQUEsV0FBVyxFQUFFQSxXQUFmO0FBQTRCQyxVQUFBQSxVQUFVLEVBQUVBO0FBQXhDLFNBSEg7QUFJSDFCLFFBQUFBLE9BQU8sRUFBRSxpQkFBU0MsTUFBVCxFQUFpQjtBQUN0QixjQUFJQSxNQUFNLElBQUksSUFBZCxFQUFvQjtBQUNoQmQsWUFBQUEsSUFBSSxDQUFDQyxJQUFMLENBQVU7QUFDTkMsY0FBQUEsUUFBUSxFQUFFLFNBREo7QUFFTkosY0FBQUEsSUFBSSxFQUFFLFNBRkE7QUFHTkMsY0FBQUEsS0FBSyxFQUFFLHlCQUhEO0FBSU5JLGNBQUFBLGlCQUFpQixFQUFFLEtBSmI7QUFLTkMsY0FBQUEsS0FBSyxFQUFFO0FBTEQsYUFBVjtBQU9BZCxZQUFBQSxRQUFRLENBQUN5QixRQUFULEdBQW9CLFNBQXBCO0FBQ0gsV0FURCxNQVNPLElBQUlELE1BQU0sSUFBSSxJQUFkLEVBQW9CO0FBQ3ZCZCxZQUFBQSxJQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNOQyxjQUFBQSxRQUFRLEVBQUUsU0FESjtBQUVOSixjQUFBQSxJQUFJLEVBQUUsT0FGQTtBQUdOQyxjQUFBQSxLQUFLLEVBQUUsNkJBSEQ7QUFJTkksY0FBQUEsaUJBQWlCLEVBQUUsS0FKYjtBQUtOQyxjQUFBQSxLQUFLLEVBQUU7QUFMRCxhQUFWO0FBT0gsV0FSTSxNQVFBO0FBQ0hKLFlBQUFBLElBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ05DLGNBQUFBLFFBQVEsRUFBRSxTQURKO0FBRU5KLGNBQUFBLElBQUksRUFBRSxPQUZBO0FBR05DLGNBQUFBLEtBQUssRUFBRSx3QkFIRDtBQUlOSSxjQUFBQSxpQkFBaUIsRUFBRSxLQUpiO0FBS05DLGNBQUFBLEtBQUssRUFBRTtBQUxELGFBQVY7QUFPSDtBQUNKLFNBL0JFO0FBZ0NIWSxRQUFBQSxLQUFLLEVBQUUsaUJBQVcsQ0FBRTtBQWhDakIsT0FBUDtBQWtDSDtBQUNKLEdBaEREO0FBbURBM0IsRUFBQUEsQ0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JHLEVBQXhCLENBQTJCLE1BQTNCLEVBQW1DLFlBQVc7QUFDMUMsUUFBSWdELE9BQU8sQ0FBQ25ELENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLEdBQVIsRUFBRCxDQUFQLElBQTBCLEtBQTlCLEVBQXFDO0FBQ2pDakIsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNkMsUUFBUixDQUFpQixZQUFqQjtBQUNBN0MsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRb0QsTUFBUjtBQUNILEtBSEQsTUFHTztBQUNIcEQsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEMsV0FBUixDQUFvQixZQUFwQixFQUFrQ0QsUUFBbEMsQ0FBMkMsVUFBM0M7QUFDSDtBQUNKLEdBUEQ7O0FBU0EsV0FBU3JDLGFBQVQsQ0FBdUJpQyxTQUF2QixFQUFrQztBQUM5QixRQUFJQyxLQUFLLEdBQUcsQ0FBWjtBQUNBLFFBQUlDLE1BQU0sR0FBRzNDLENBQUMsQ0FBQ3lDLFNBQVMsR0FBRyxRQUFiLENBQWQ7QUFDQXpDLElBQUFBLENBQUMsQ0FBQzRDLElBQUYsQ0FBT0QsTUFBUCxFQUFlLFlBQVc7QUFDdEIsVUFBSTNDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLEdBQVIsR0FBY3VCLE1BQWQsS0FBeUIsQ0FBN0IsRUFBZ0M7QUFDNUJFLFFBQUFBLEtBQUs7QUFDTDFDLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZDLFFBQVIsQ0FBaUIsWUFBakI7QUFDSCxPQUhELE1BR087QUFDSDdDLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUThDLFdBQVIsQ0FBb0IsWUFBcEIsRUFBa0NELFFBQWxDLENBQTJDLFVBQTNDO0FBQ0g7QUFDSixLQVBEO0FBUUEsV0FBT0gsS0FBSyxJQUFJLENBQVQsR0FBYSxJQUFiLEdBQW9CLEtBQTNCO0FBQ0g7O0FBRUQsV0FBU1MsT0FBVCxDQUFpQkUsS0FBakIsRUFBd0I7QUFDcEIsUUFBSUMsS0FBSyxHQUFHLG1FQUFaOztBQUNBLFFBQUksQ0FBQ0EsS0FBSyxDQUFDQyxJQUFOLENBQVdGLEtBQVgsQ0FBTCxFQUF3QjtBQUNwQixhQUFPLEtBQVA7QUFDSCxLQUZELE1BRU87QUFDSCxhQUFPLElBQVA7QUFDSDtBQUNKO0FBQ0osQ0EvS0Q7Ozs7Ozs7Ozs7OztBQ0FBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYXJ0aWNsZS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY2xpZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtZnJlZS9jc3MvYWxsLm1pbi5jc3MiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gYXNzZXRzL2pzL2FwcC5qc1xyXG4vKlxyXG4gKiBXZWxjb21lIHRvIHlvdXIgYXBwJ3MgbWFpbiBKYXZhU2NyaXB0IGZpbGUhXHJcbiAqXHJcbiAqIFdlIHJlY29tbWVuZCBpbmNsdWRpbmcgdGhlIGJ1aWx0IHZlcnNpb24gb2YgdGhpcyBKYXZhU2NyaXB0IGZpbGVcclxuICogKGFuZCBpdHMgQ1NTIGZpbGUpIGluIHlvdXIgYmFzZSBsYXlvdXQgKGJhc2UuaHRtbC50d2lnKS5cclxuICovXHJcblxyXG4vLyBhbnkgQ1NTIHlvdSBpbXBvcnQgd2lsbCBvdXRwdXQgaW50byBhIHNpbmdsZSBjc3MgZmlsZSAoYXBwLmNzcyBpbiB0aGlzIGNhc2UpXHJcblxyXG4vLyBOZWVkIGpRdWVyeT8gSW5zdGFsbCBpdCB3aXRoIFwieWFybiBhZGQganF1ZXJ5XCIsIHRoZW4gdW5jb21tZW50IHRvIGltcG9ydCBpdC5cclxuLy8gaW1wb3J0ICQgZnJvbSAnanF1ZXJ5JztcclxuXHJcbnJlcXVpcmUoJy4uL2pzL2NsaWVudC5qcycpO1xyXG5yZXF1aXJlKCcuLi9qcy9hcnRpY2xlLmpzJyk7XHJcbnJlcXVpcmUoJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1mcmVlL2Nzcy9hbGwubWluLmNzcycpO1xyXG5cclxuY29uc29sZS5sb2coJ0hlbGxvIFdlYnBhY2sgRW5jb3JlJyk7IiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgJChcIiNidG5fYWpvdXRfYXJ0aWNsZVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQoJyNtb2RhbF9ham91dF9hcnRpY2xlJykubW9kYWwoeyBiYWNrZHJvcDogJ3N0YXRpYycsIGtleWJvYXJkOiBmYWxzZSB9KTtcclxuICAgIH0pXHJcblxyXG4gICAgJChcIiNidG5fdmFsaWRfYWpvdXRfYXJ0aWNsZVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCBjaGVja19pbnB1dCA9IGFsbF9pbnB1dF9zZXQoXCIjbW9kYWxfYWpvdXRfYXJ0aWNsZVwiKVxyXG4gICAgICAgIGxldCBpY29uID0gXCJlcnJvclwiXHJcbiAgICAgICAgbGV0IHRpdGxlID0gXCJBam91dCBhcnRpY2xlIMOpY2jDqGNcIlxyXG4gICAgICAgIGlmICghY2hlY2tfaW5wdXQpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InLFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdDaGFtcHMgb2JsaWdhdG9pcmUgdmlkZScsXHJcbiAgICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB0aW1lcjogMjAwMFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgYXJ0aWNsZV9ub20gPSAkKFwiI2lucHV0X2FydGljbGVfbm9tXCIpLnZhbCgpXHJcbiAgICAgICAgbGV0IGFydGljbGVfcHJpeCA9ICQoXCIjaW5wdXRfYXJ0aWNsZV9wcml4XCIpLnZhbCgpXHJcbiAgICAgICAgbGV0IGFydGljbGVfZGVzY3JpcHRpb24gPSAkKFwiI2lucHV0X2FydGljbGVfZGVzY3JpcHRpb25cIikudmFsKClcclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB0eXBlOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgdXJsOiBcInZhbGlkYXRpb25fYWpvdXRfYXJ0aWNsZVwiLFxyXG4gICAgICAgICAgICBkYXRhOiB7IGFydGljbGVfbm9tOiBhcnRpY2xlX25vbSwgYXJ0aWNsZV9wcml4OiBhcnRpY2xlX3ByaXgsIGFydGljbGVfZGVzY3JpcHRpb246IGFydGljbGVfZGVzY3JpcHRpb24gfSxcclxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24ocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0ID09IFwiMVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbiA9IFwic3VjY2Vzc1wiXHJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGUgPSBcIkFydGljbGUgYWpvdXTDqVwiXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBTd2FsLmZpcmUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogaWNvbixcclxuICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRpbWVyOiAyMDAwXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQubG9jYXRpb24gPSBkb2N1bWVudC5sb2NhdGlvblxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7fVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSlcclxuXHJcbiAgICAkKFwiLmJ0bl9zdXBwcl9hcnRpY2xlXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCBpY29uID0gXCJlcnJvclwiXHJcbiAgICAgICAgbGV0IHRpdGxlID0gXCJFY2jDqGMgc3VwcHJlc3Npb24gYXJ0aWNsZVwiXHJcbiAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgdGl0bGU6ICdTdXBwcmltw6kgdW4gYXJ0aWNsZScsXHJcbiAgICAgICAgICAgIHRleHQ6IFwiw4p0ZXMtdm91cyBzw7tyIGRlIHZvdWxvaXIgc3VwcHJpbWVyP1wiLFxyXG4gICAgICAgICAgICBpY29uOiAnd2FybmluZycsXHJcbiAgICAgICAgICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXHJcbiAgICAgICAgICAgIGNvbmZpcm1CdXR0b25Db2xvcjogJyMzMDg1ZDYnLFxyXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25Db2xvcjogJyNkMzMnLFxyXG4gICAgICAgICAgICBjb25maXJtQnV0dG9uVGV4dDogJ09VSScsXHJcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvblRleHQ6ICdOT04nLFxyXG4gICAgICAgIH0pLnRoZW4oKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0LmlzQ29uZmlybWVkKSB7XHJcbiAgICAgICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIHVybDogXCJzdXBwcmVzaW9uX2FydGljbGVcIixcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7IGFydGljbGVfaWQ6ICQodGhpcykudmFsKCkgfSxcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhID09IFwiMVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uID0gXCJzdWNjZXNzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlID0gXCJBcnRpY2xlIHN1cHByaW3DqVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBpY29uLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IHRpdGxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGltZXI6IDIwMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQubG9jYXRpb24gPSBkb2N1bWVudC5sb2NhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge31cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgIH0pXHJcblxyXG4gICAgJChcIi5idG5fbW9kaWZfYXJ0aWNsZVwiKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyBsZXQgaWNvbiA9IFwiZXJyb3JcIlxyXG4gICAgICAgIC8vIGxldCB0aXRsZSA9IFwiRWNow6hjIHLDqWN1cMOpcmF0aW9uIGFydGljbGVcIlxyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICB1cmw6IFwibW9kaWZpY2F0aW9uX2FydGljbGVcIixcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgICAgICAgZGF0YTogeyBhcnRpY2xlX2lkOiAkKHRoaXMpLnZhbCgpIH0sXHJcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKE9iamVjdC5rZXlzKGRhdGEpLmxlbmd0aClcclxuICAgICAgICAgICAgICAgIGlmIChPYmplY3Qua2V5cyhkYXRhKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YS5hcnRpY2xlX2Rlc2NyaXB0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICQoJyNpbnB1dF9tb2RpZl9hcnRpY2xlX25vbScpLnZhbChkYXRhLmFydGljbGVfbm9tKVxyXG4gICAgICAgICAgICAgICAgICAgICQoJyNpbnB1dF9tb2RpZl9hcnRpY2xlX3ByaXgnKS52YWwoZGF0YS5hcnRpY2xlX3ByaXgpXHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI2lucHV0X21vZGlmX2FydGljbGVfZGVzY3JpcHRpb24nKS52YWwoZGF0YS5hcnRpY2xlX2Rlc2NyaXB0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICQoJyNidG5fdmFsaWRfbW9kaWZfYXJ0aWNsZScpLnZhbChkYXRhLmFydGljbGVfaWQpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICQoJyNtb2RhbF9tb2RpZl9hcnRpY2xlJykubW9kYWwoeyBiYWNrZHJvcDogJ3N0YXRpYycsIGtleWJvYXJkOiBmYWxzZSB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWNvbiA9IFwic3VjY2Vzc1wiXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGl0bGUgPSBcIkFydGljbGUgbW9kaWZpw6lcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgcG9zaXRpb246ICd0b3AtZW5kJyxcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgaWNvbjogaWNvbixcclxuICAgICAgICAgICAgICAgIC8vICAgICAgICAgdGl0bGU6IHRpdGxlLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgIHRpbWVyOiAyMDAwXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgfSlcclxuICAgICAgICAgICAgICAgIC8vIGRvY3VtZW50LmxvY2F0aW9uID0gZG9jdW1lbnQubG9jYXRpb25cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge31cclxuICAgICAgICB9KTtcclxuICAgIH0pXHJcblxyXG5cclxuICAgICQoXCIjYnRuX3ZhbGlkX21vZGlmX2FydGljbGVcIikub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgbGV0IGljb24gPSBcImVycm9yXCJcclxuICAgICAgICBsZXQgdGl0bGUgPSBcIkVjaMOoYyBtb2RpZmljYXRpb24gYXJ0aWNsZVwiXHJcblxyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICB1cmw6IFwidmFsaWRhdGlvbl9tb2RpZl9hcnRpY2xlXCIsXHJcbiAgICAgICAgICAgIGRhdGE6IHsgYXJ0aWNsZV9pZDogJCh0aGlzKS52YWwoKSwgYXJ0aWNsZV9ub206ICQoJyNpbnB1dF9tb2RpZl9hcnRpY2xlX25vbScpLnZhbCgpLCBhcnRpY2xlX3ByaXg6ICQoJyNpbnB1dF9tb2RpZl9hcnRpY2xlX3ByaXgnKS52YWwoKSwgYXJ0aWNsZV9kZXNjcmlwdGlvbjogJCgnI2lucHV0X21vZGlmX2FydGljbGVfZGVzY3JpcHRpb24nKS52YWwoKSB9LFxyXG4gICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSA9PSBcIjFcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIGljb24gPSBcInN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlID0gXCJBcnRpY2xlIG1vZGlmacOpXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICd0b3AtZW5kJyxcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiBpY29uLFxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgdGltZXI6IDIwMDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5sb2NhdGlvbiA9IGRvY3VtZW50LmxvY2F0aW9uXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHt9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KVxyXG5cclxuICAgIGZ1bmN0aW9uIGFsbF9pbnB1dF9zZXQoaWRfcGFyZW50KSB7XHJcbiAgICAgICAgbGV0IGVtcHR5ID0gMDtcclxuICAgICAgICBsZXQgaW5wdXRzID0gJChpZF9wYXJlbnQgKyBcIiBpbnB1dFwiKTtcclxuICAgICAgICAkLmVhY2goaW5wdXRzLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBlbXB0eSsrO1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImlzLWludmFsaWRcIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwiaXMtaW52YWxpZFwiKS5hZGRDbGFzcyhcImlzLXZhbGlkXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGVtcHR5ID09IDAgPyB0cnVlIDogZmFsc2VcclxuICAgIH1cclxuXHJcbn0pIiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICAkKCcjbGlua19yZWdpc3RyYXRpb24nKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XHJcblxyXG4gICAgICAgICQoJyNtb2RhbF9yZWdpc3RyYXRpb25fY2xpZW50JykubW9kYWwoeyBiYWNrZHJvcDogJ3N0YXRpYycsIGtleWJvYXJkOiBmYWxzZSB9KTtcclxuICAgICAgICAvLyBTd2FsLmZpcmUoJ0FueSBmb29sIGNhbiB1c2UgYSBjb21wdXRlcicpXHJcblxyXG4gICAgICAgIC8vIGNvbnN0IHsgdmFsdWU6IGZvcm1WYWx1ZXMgfSA9IFN3YWwuZmlyZSh7XHJcbiAgICAgICAgLy8gICAgIGhlaWdodEF1dG86IHRydWUsXHJcbiAgICAgICAgLy8gICAgIGN1c3RvbUNsYXNzOiB7XHJcbiAgICAgICAgLy8gICAgICAgICBjb250ZW50OiAnY29udGVudC1zd2VldC1hbGVydCcsXHJcbiAgICAgICAgLy8gICAgICAgICBodG1sQ29udGFpbmVyOiAndGVzdC1odG1sQ29udGFpbmVyLWNsYXNzJyxcclxuICAgICAgICAvLyAgICAgICAgIGNvbnRhaW5lcjogJ3Rlc3QtY29udGFpbmVyLWNsYXNzJyxcclxuICAgICAgICAvLyAgICAgICAgIHBvcHVwOiAndGVzdC1wb3B1cC1jbGFzcydcclxuICAgICAgICAvLyAgICAgfSxcclxuICAgICAgICAvLyAgICAgdGl0bGU6ICdFbnJlZ2lzdHJlbWVudCB1dGlsaXNhdGV1cicsXHJcbiAgICAgICAgLy8gICAgIGh0bWw6ICc8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3dcIj4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8bGFiZWwgZm9yPVwiaW5wdXRfbWFpbFwiIGNsYXNzPVwiY29sLW1kLTQgY29sLWZvcm0tbGFiZWxcIj5BZHJlc3NlIG1haWw8L2xhYmVsPicgK1xyXG4gICAgICAgIC8vICAgICAgICAgJzxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPicgK1xyXG4gICAgICAgIC8vICAgICAgICAgJzxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJpbnB1dF9tYWlsXCIgcGxhY2Vob2xkZXI9XCJWb3RyZSBhZHJlc3NlIG1haWxcIj4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8L2Rpdj4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCByb3cgbWItMFwiPicgK1xyXG4gICAgICAgIC8vICAgICAgICAgJzxsYWJlbCBmb3I9XCJpbnB1dF9tZHBcIiBjbGFzcz1cImNvbC1tZC00IGNvbC1mb3JtLWxhYmVsXCI+TW90IGRlIHBhc3NlPC9sYWJlbD4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8ZGl2IGNsYXNzPVwiY29sLW1kLThcIj4nICtcclxuICAgICAgICAvLyAgICAgICAgICc8aW5wdXQgdHlwZT1cInBhc3N3b3JkXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImlucHV0X21kcFwiIHBsYWNlaG9sZGVyPVwiVm90cmUgbW90IGRlIHBhc3NlXCI+JyArXHJcbiAgICAgICAgLy8gICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgLy8gICAgICAgICAnPC9kaXY+JyArXHJcbiAgICAgICAgLy8gICAgICAgICAnPC9kaXY+JyxcclxuICAgICAgICAvLyAgICAgZm9jdXNDb25maXJtOiBmYWxzZSxcclxuICAgICAgICAvLyAgICAgcHJlQ29uZmlybTogKCkgPT4ge1xyXG4gICAgICAgIC8vICAgICAgICAgcmV0dXJuIFtcclxuICAgICAgICAvLyAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3dhbC1pbnB1dDEnKS52YWx1ZSxcclxuICAgICAgICAvLyAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3dhbC1pbnB1dDInKS52YWx1ZVxyXG4gICAgICAgIC8vICAgICAgICAgXVxyXG4gICAgICAgIC8vICAgICB9XHJcbiAgICAgICAgLy8gfSlcclxuXHJcbiAgICAgICAgLy8gaWYgKGZvcm1WYWx1ZXMpIHtcclxuICAgICAgICAvLyAgICAgU3dhbC5maXJlKEpTT04uc3RyaW5naWZ5KGZvcm1WYWx1ZXMpKVxyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICB9KVxyXG5cclxuICAgICQoJyNidG5fdmFsaWRfaW5zY3JpcHRpb25fY2xpZW50Jykub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgY2hlY2tfaW5wdXQgPSBhbGxfaW5wdXRfc2V0KFwiI21vZGFsX3JlZ2lzdHJhdGlvbl9jbGllbnRcIilcclxuICAgICAgICBpZiAoIWNoZWNrX2lucHV0KSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ3RvcC1lbmQnLFxyXG4gICAgICAgICAgICAgICAgaWNvbjogJ2Vycm9yJyxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiAnQ2hhbXBzIG9ibGlnYXRvaXJlIHZpZGUnLFxyXG4gICAgICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdGltZXI6IDIwMDBcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnbWFpbCBubyB2aWRlJylcclxuICAgICAgICAgICAgbGV0IGNsaWVudF9ub20gPSAkKFwiI2lucHV0X25vbV9jbGllbnRcIikudmFsKClcclxuICAgICAgICAgICAgbGV0IGNsaWVudF9tYWlsID0gJChcIiNpbnB1dF9tYWlsX2NsaWVudFwiKS52YWwoKVxyXG4gICAgICAgICAgICBsZXQgY2xpZW50X21kcCA9ICQoXCIjaW5wdXRfbWRwX2NsaWVudFwiKS52YWwoKVxyXG5cclxuICAgICAgICAgICAgaWYgKElzRW1haWwoY2xpZW50X21haWwpID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKFwiaXMtaW52YWxpZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdtYWlsIHZhbGlkZScpXHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwiaXMtaW52YWxpZFwiKS5hZGRDbGFzcyhcImlzLXZhbGlkXCIpO1xyXG4gICAgICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IFwidmFsaWRhdGlvbl9pbnNjcmlwdGlvbl9jbGllbnRcIixcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7IGNsaWVudF9ub206IGNsaWVudF9ub20sIGNsaWVudF9tYWlsOiBjbGllbnRfbWFpbCwgY2xpZW50X21kcDogY2xpZW50X21kcCB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0ID09IFwiMVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uID0gXCJzdWNjZXNzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlID0gXCJDbGllbnQgYWpvdXTDqVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uID0gXCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZSA9IFwiQWRyZXNzZSBtYWlsIGV4aXN0ZSBkw6lqw6BcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ3RvcC1lbmQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogaWNvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dDb25maXJtQnV0dG9uOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyOiAyMDAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmxvY2F0aW9uID0gZG9jdW1lbnQubG9jYXRpb25cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHt9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgfSlcclxuXHJcbiAgICAkKCcjYnRuX2F1dGhlbnRpZmljYXRpb24nKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICBsZXQgY2hlY2tfaW5wdXQgPSBhbGxfaW5wdXRfc2V0KFwiI2NvbnRlbnRfYXV0aGVudGlmaWNhdGlvblwiKVxyXG4gICAgICAgIGlmICghY2hlY2tfaW5wdXQpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICBpY29uOiAnZXJyb3InLFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdDaGFtcHMgb2JsaWdhdG9pcmUgdmlkZScsXHJcbiAgICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB0aW1lcjogMjAwMFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCBjbGllbnRfbWFpbCA9ICQoJyNpbnB1dF9tYWlsJykudmFsKClcclxuICAgICAgICAgICAgbGV0IGNsaWVudF9tZHAgPSAkKCcjaW5wdXRfbWRwJykudmFsKClcclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgdXJsOiBcImF1dGhlbnRpZmljYXRpb25fY2xpZW50XCIsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7IGNsaWVudF9tYWlsOiBjbGllbnRfbWFpbCwgY2xpZW50X21kcDogY2xpZW50X21kcCB9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24ocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdCA9PSBcIm9rXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBcInN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkF1dGhlbnRpZmljYXRpb24gdmFsaWTDqVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGltZXI6IDIwMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQubG9jYXRpb24gPSBcImFydGljbGVcIlxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzdWx0ID09IFwia29cIikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBTd2FsLmZpcmUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICd0b3AtZW5kJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGljb246IFwiZXJyb3JcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIkF1dGhlbnRpZmljYXRpb24gbm9uIHZhbGlkw6lcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dDb25maXJtQnV0dG9uOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVyOiAyMDAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBcImVycm9yXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJVdGlsaXNhdGV1ciBub24gY2xpZW50XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aW1lcjogMjAwMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7fVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KVxyXG5cclxuXHJcbiAgICAkKCcjaW5wdXRfbWFpbF9jbGllbnQnKS5vbignYmx1cicsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmIChJc0VtYWlsKCQodGhpcykudmFsKCkpID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJpcy1pbnZhbGlkXCIpO1xyXG4gICAgICAgICAgICAkKHRoaXMpLmZhZGVJbigpXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhcImlzLWludmFsaWRcIikuYWRkQ2xhc3MoXCJpcy12YWxpZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9KVxyXG5cclxuICAgIGZ1bmN0aW9uIGFsbF9pbnB1dF9zZXQoaWRfcGFyZW50KSB7XHJcbiAgICAgICAgbGV0IGVtcHR5ID0gMDtcclxuICAgICAgICBsZXQgaW5wdXRzID0gJChpZF9wYXJlbnQgKyBcIiBpbnB1dFwiKTtcclxuICAgICAgICAkLmVhY2goaW5wdXRzLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBlbXB0eSsrO1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImlzLWludmFsaWRcIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwiaXMtaW52YWxpZFwiKS5hZGRDbGFzcyhcImlzLXZhbGlkXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGVtcHR5ID09IDAgPyB0cnVlIDogZmFsc2VcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBJc0VtYWlsKGVtYWlsKSB7XHJcbiAgICAgICAgdmFyIHJlZ2V4ID0gL14oW2EtekEtWjAtOV9cXC5cXC1cXCtdKStcXEAoKFthLXpBLVowLTlcXC1dKStcXC4pKyhbYS16QS1aMC05XXsyLDR9KSskLztcclxuICAgICAgICBpZiAoIXJlZ2V4LnRlc3QoZW1haWwpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307Il0sIm5hbWVzIjpbInJlcXVpcmUiLCJjb25zb2xlIiwibG9nIiwiJCIsImRvY3VtZW50IiwicmVhZHkiLCJvbiIsIm1vZGFsIiwiYmFja2Ryb3AiLCJrZXlib2FyZCIsImNoZWNrX2lucHV0IiwiYWxsX2lucHV0X3NldCIsImljb24iLCJ0aXRsZSIsIlN3YWwiLCJmaXJlIiwicG9zaXRpb24iLCJzaG93Q29uZmlybUJ1dHRvbiIsInRpbWVyIiwiYXJ0aWNsZV9ub20iLCJ2YWwiLCJhcnRpY2xlX3ByaXgiLCJhcnRpY2xlX2Rlc2NyaXB0aW9uIiwiYWpheCIsInR5cGUiLCJ1cmwiLCJkYXRhIiwic3VjY2VzcyIsInJlc3VsdCIsImxvY2F0aW9uIiwiZXJyb3IiLCJ0ZXh0Iiwic2hvd0NhbmNlbEJ1dHRvbiIsImNvbmZpcm1CdXR0b25Db2xvciIsImNhbmNlbEJ1dHRvbkNvbG9yIiwiY29uZmlybUJ1dHRvblRleHQiLCJjYW5jZWxCdXR0b25UZXh0IiwidGhlbiIsImlzQ29uZmlybWVkIiwiYXJ0aWNsZV9pZCIsImRhdGFUeXBlIiwiT2JqZWN0Iiwia2V5cyIsImxlbmd0aCIsImlkX3BhcmVudCIsImVtcHR5IiwiaW5wdXRzIiwiZWFjaCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJlIiwiY2xpZW50X25vbSIsImNsaWVudF9tYWlsIiwiY2xpZW50X21kcCIsIklzRW1haWwiLCJmYWRlSW4iLCJlbWFpbCIsInJlZ2V4IiwidGVzdCJdLCJzb3VyY2VSb290IjoiIn0=